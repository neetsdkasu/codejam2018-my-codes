package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var scn = bufio.NewScanner(os.Stdin)

func init() {
	const sz = 1e7
	scn.Buffer(make([]byte, sz), sz)
}

func gets() []int64 {
	scn.Scan()
	s := scn.Text()
	ss := strings.Split(s, " ")
	xs := make([]int64, len(ss))
	for i, t := range ss {
		xs[i], _ = strconv.ParseInt(t, 10, 64)
	}
	return xs
}

func main() {
	t := int(gets()[0])
	for i := 0; i < t; i++ {
		ans := solve()
		fmt.Printf("Case #%d: %d", i+1, ans)
		fmt.Println()
	}
}

func solve() int {
	_ = int(gets()[0])
	xs := gets()
	hs := make(map[int]int64)
	hs[0] = 0
	for _, x := range xs {
		ts := make(map[int]int64)
		for k, c := range hs {
			if e, ok := ts[k]; ok {
				if c < e {
					ts[k] = c
				}
			} else {
				ts[k] = c
			}
			if c <= x*6 {
				d := k + 1
				w := c + x
				if e, ok := ts[d]; ok {
					if w < e {
						ts[d] = w
					}
				} else {
					ts[d] = w
				}
			}
		}
		hs = ts
	}
	ans := 1
	for c := range hs {
		if c > ans {
			ans = c
		}
	}
	return ans
}
