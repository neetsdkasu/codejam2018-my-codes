package main

import (
	"fmt"
)

func main() {
	var t int
	fmt.Scan(&t)
	for i := 0; i < t; i++ {
		var n, l int
		fmt.Scan(&n, &l)
		ss := make([]string, n)
		for j := 0; j < n; j++ {
			fmt.Scan(&ss[j])
		}
		ans := solve(n, l, ss)
		fmt.Printf("Case #%d: %s", i+1, ans)
		fmt.Println()
	}
}

func solve(n, l int, ss []string) string {
	if n == 1 || l == 1 {
		return "-"
	}
	hs := make(map[string]bool)
	xs := make([]map[byte]int, l)
	for i := range xs {
		xs[i] = make(map[byte]int)
	}
	for _, s := range ss {
		hs[s] = true
		bs := []byte(s)
		for j, c := range bs {
			if x, ok := xs[j][c]; ok {
				xs[j][c] = x + 1
			} else {
				xs[j][c] = 1
			}
		}
	}
	if ans, ok := dfs(hs, xs, 0, make([]byte, 0, l)); ok {
		return ans
	}
	return "-"
}
func dfs(hs map[string]bool, xs []map[byte]int, i int, ts []byte) (string, bool) {
	if i >= len(xs) {
		ans := string(ts)
		if _, ok := hs[ans]; ok {
			return "", false
		}
		return ans, true
	}
	for k, _ := range xs[i] {
		es := append(ts, k)
		if ans, ok := dfs(hs, xs, i+1, es); ok {
			return ans, ok
		}
	}
	return "", false
}
