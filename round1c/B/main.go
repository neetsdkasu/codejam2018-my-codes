package main

import (
	"fmt"
)

func main() {
	var t int
	fmt.Scan(&t)
	for i := 0; i < t; i++ {
		solve()
	}
}

func solve() {
	var n int
	fmt.Scan(&n)
	hs := make([]bool, n+n)
	ws := make([]int, n+n)
	for i := 0; i < n; i++ {
		var d int
		fmt.Scan(&d)
		fs := make([]int, d)
		for j := 0; j < d; j++ {
			fmt.Scan(&fs[j])
		}
		ans := -1
		for _, x := range fs {
			ws[x]++
			if hs[x] {
				continue
			}
			if ans < 0 {
				ans = x
				continue
			}
			if ws[x] <= ws[ans] {
				ans = x
			}
		}
		if ans >= 0 {
			hs[ans] = true
		}
		fmt.Println(ans)
	}
}
