package main

import (
	"fmt"
	"sort"
)

var xs = []int{}

func init() {
	for i := 0; i <= 500; i++ {
		for j := 0; j <= 500; j++ {
			k := i + j
			x := (((k << 10) | i) << 10) | j
			xs = append(xs, x)
		}
	}
	sort.Ints(xs)
	fmt.Println(len(xs))
}

func main() {

	var t int
	fmt.Scan(&t)
	for i := 0; i < t; i++ {
		fmt.Printf("Case #%d: ", i+1)
		var r, b int
		fmt.Scan(&r, &b)
		ans := solve(r, b)
		fmt.Println(ans)
	}
}

func solve(r, b int) (ret int) {
	if r == 0 || b == 0 {
		r = r + b
		for i := 1; ; i++ {
			if r < i {
				break
			}
			r -= i
			ret++
		}
		return
	}
	if r > b {
		r, b = b, r
	}
	for _, x := range xs {
		j := x & 1023
		i := (x >> 10) & 1023
		if r >= i && b >= j {
			r -= i
			b -= j
			ret++
			if r == 0 || b == 0 {
				break
			}
		}
	}
	return
}
