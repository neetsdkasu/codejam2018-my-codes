package main

import (
	"fmt"

)

func main() {
	var t int
	fmt.Scan(&t)
	for i := 0; i < t; i++ {
		fmt.Printf("Case #%d: ", i+1)
		var n int
		fmt.Scan(&n)
		xs := make([][]int, n)
		for i := range xs {
			xs[i] = make([]int, n)
			for j := range xs[i] {
				fmt.Scan(&xs[i][j])
			}
		}
		ans := solve(n, xs)
		fmt.Println(ans)
	}
}

func solve(n int, xs [][]int) (ret int) {

	ho := make([]map[int]int, n)
	for i := range ho {
		ho[i] = make(map[int]int)
	}
	ve := make([]map[int]int, n)
	for i := range ve {
		ve[i] = make(map[int]int)
	}
	for i, vs := range xs {
		for j, v := range vs {
			if c, ok := ho[i][v]; ok {
				ho[i][v] = c + 1
			} else {
				ho[i][v] = 1
			}
			if c, ok := ve[j][v]; ok {
				ve[j][v] = c + 1
			} else {
				ve[j][v] = 1
			}
		}
	}
	zs := []int{}
	for i, vs := range xs {
		for j, v := range vs {
			e := (ho[i][v] + ve[j][v])
			if e == 2 {
				continue
			}
			z := (i << 8) | j
			zs = append(zs, z)
		}
	}
	const M = (1 << 8) - 1
	for len(zs) > 0 {
		me := 0
		mp := -1
		ts := []int{}
		for _, z := range zs {
			j := z & M
			i := (z >> 8) & M
			v := xs[i][j]
			e := ho[i][v] + ve[j][v]
			if e < 3 {
				continue
			}
			ts = append(ts, z)
			if e > me {
				me = e
				mp = len(ts) - 1
			}
		}
		if mp < 0 {
			break
		}
		z := ts[mp]
		ts[mp] = ts[0]
		zs = ts[1:]
		j := z & M
		i := (z >> 8) & M
		v := xs[i][j]
		ho[i][v]--
		ve[j][v]--
		ret++
	}

	return
}
