package main

import (
	"fmt"
)

func main() {
	var t int
	fmt.Scan(&t)
	for i := 0; i < t; i++ {
		fmt.Printf("Case #%d: ", i+1)
		var c int
		fmt.Scan(&c)
		xs := make([]int, c)
		for i := range xs {
			fmt.Scan(&xs[i])
		}
		if ans, ok := solve(c, xs); ok {
			fmt.Println(len(ans))
			for _, vs := range ans {
				for _, v := range vs {
					switch v {
					case 0:
						fmt.Print(".")
					case 1:
						fmt.Print("/")
					default:
						fmt.Print("\\")
					}
				}
				fmt.Println()
			}
		} else {
			fmt.Println("IMPOSSIBLE")
		}
	}
}

func solve(c int, xs []int) (ret [][]int, ok bool) {
	if xs[0] == 0 || xs[c-1] == 0 {
		return
	}
	allone := true
	for _, x := range xs {
		if x != 1 {
			allone = false
			break
		}
	}
	if allone {
		ret = append(ret, make([]int, c))
		ok = true
		return
	}
	ret = make([][]int, c+1)
	for i := range ret {
		ret[i] = make([]int, c)
	}
	p := 0
	for i, x := range xs {
		if x == 0 {
			continue
		}
		if p < i {
			e := i - p
			for j := p; j < i; j++ {
				ret[j-p][j] = 2
			}
			if x <= e {
				p += x
				x = 0
			} else {
				p = i
				x -= e
			}
		}
		if x > 0 && p == i {
			p++
			x--
		}
		if x > 0 && p > i {
			w := p + x
			for j := i + 1; j < w; j++ {
				ret[w-j-1][j] = 1
			}
			p = w
		}
	}
	h := 0
	for i := 0; i < c; i++ {
		for j := len(ret) - 1; j >= 0; j-- {
			if ret[j][i] != 0 {
				if j > h {
					h = j
				}
				break
			}
		}
	}
	h++
	ret = ret[:h+1]
	ok = true
	return
}
