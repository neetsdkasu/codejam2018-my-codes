package main

import (
	"fmt"
	"math/rand"
	"testing"
	"time"
)

func TestSolve(t *testing.T) {
	seed := time.Now().Unix()
	rand.Seed(seed)
	for e := 0; e < 10; e++ {
		fmt.Println("----------------------------")
		c := rand.Intn(10) + 10
		xs := make([]int, c)
		xs[0] = 1
		xs[c-1] = 1
		for i := 2; i < c; i++ {
			p := rand.Intn(c)
			xs[p]++
		}
		fmt.Println(c)
		fmt.Println(xs)
		ans, ok := solve(c, xs)
		output(ans, ok)
		if !ok {
			continue
		}
		bs := make([]int, c)
		for i := range bs {
			bs[i] = 1
		}
		for _, vs := range ans {
			cs := make([]int, c)
			for i, v := range vs {
				switch v {
				case 0:
					cs[i] += bs[i]
				case 1:
					cs[i-1] += bs[i]
				default:
					cs[i+1] += bs[i]
				}
			}
			bs = cs
		}
		check := true
		for i, x := range xs {
			if x != bs[i] {
				check = false
				break
			}
		}
		if !check {
			fmt.Println(bs)
			t.Fatal("wrong")
		}
	}
}

func output(ans [][]int, ok bool) {
	if ok {
		fmt.Println(len(ans))
		for _, vs := range ans {
			for _, v := range vs {
				switch v {
				case 0:
					fmt.Print(".")
				case 1:
					fmt.Print("/")
				default:
					fmt.Print("\\")
				}
			}
			fmt.Println()
		}
	} else {
		fmt.Println("IMPOSSIBLE")
	}
}
