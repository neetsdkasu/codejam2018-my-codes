CodeJam2018
===========

Results:   
https://codejam.withgoogle.com/2018/challenges   



### Qualification Round  

Rank: 791  
Score: 90  
A ... set1: 5pt,  set2: 0pt  
B ... set1: 8pt,  set2: 15pt  
C ... set1: 10pt, set2: 20pt  
D ... set1: 11pt, set2: 21pt  



### Round 1B  

Rank: 2435  
Score: 0  
A ... set1: 0pt, set2: -, set3: -  
B ... set1: 0pt, set2: -  
C ... set1: 0pt, set2: -, set3: -  



### Round 1C  

Rank: 298  
Score: 100  
A ... set1: 11pt, set2: 17pt  
B ... set1: 29pt  
C ... set1: 16pt, set2: 27pt  



### Round 2  

Rank: 2706  
Score: 17  
A ... set1: 5pt, set2: 12pt  
B ... set1: 0pt, set2: -  
C ... set1: 0pt, set2: -  
D ... set1: -,   set2: -  

