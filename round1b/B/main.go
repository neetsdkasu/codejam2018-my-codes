package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type DAB struct {
	d, a, b int64
}

func (dab DAB) M() int64 { return dab.d + dab.a }
func (dab DAB) N() int64 { return dab.d - dab.b }

type Set struct {
	m, n int64
}

func solve(dab []DAB) (va, sz int64) {

	return
}

func main() {
	t := readi()
	for i := 0; i < t; i++ {
		s := readi64()
		dab := make([]DAB, s)
		for i := range dab {
			vs := readi64s()
			dab[i].d = vs[0]
			dab[i].a = vs[1]
			dab[i].b = vs[2]
		}
		va, sz := solve(dab)
		fmt.Printf("Case #%d: %d %d", i+1, va, sz)
		fmt.Println()
	}
}

var scn = bufio.NewScanner(os.Stdin)

func init() {
	const sz = 1e7
	scn.Buffer(make([]byte, sz), sz)
}

func read() string {
	scn.Scan()
	return scn.Text()
}

func readi() int {
	v, _ := strconv.Atoi(read())
	return v
}

func readi64() int64 {
	v, _ := strconv.ParseInt(read(), 10, 64)
	return v
}

func reads() []string {
	return strings.Split(read(), " ")
}

func readis() []int {
	rs := reads()
	is := make([]int, len(rs))
	for i, s := range rs {
		is[i], _ = strconv.Atoi(s)
	}
	return is
}

func readi64s() []int64 {
	rs := reads()
	is := make([]int64, len(rs))
	for i, s := range rs {
		is[i], _ = strconv.ParseInt(s, 10, 64)
	}
	return is
}
