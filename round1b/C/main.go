package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)


func solve(rs [][]int64, gs []int64) (ans int64) {

	return
}

func main() {
	t := readi()
	for i := 0; i < t; i++ {
		m := readi64()
		rs := make([][]int64, m)
		for i := range rs {
			rs[i] = readi64s()
		}
        gs := readi64s()
		ans := solve(rs, gs)
		fmt.Printf("Case #%d: %d", i+1, ans)
		fmt.Println()
	}
}

var scn = bufio.NewScanner(os.Stdin)

func init() {
	const sz = 1e7
	scn.Buffer(make([]byte, sz), sz)
}

func read() string {
	scn.Scan()
	return scn.Text()
}

func readi() int {
	v, _ := strconv.Atoi(read())
	return v
}

func readi64() int64 {
	v, _ := strconv.ParseInt(read(), 10, 64)
	return v
}

func reads() []string {
	return strings.Split(read(), " ")
}

func readis() []int {
	rs := reads()
	is := make([]int, len(rs))
	for i, s := range rs {
		is[i], _ = strconv.Atoi(s)
	}
	return is
}

func readi64s() []int64 {
	rs := reads()
	is := make([]int64, len(rs))
	for i, s := range rs {
		is[i], _ = strconv.ParseInt(s, 10, 64)
	}
	return is
}
