package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
)

type Cmp func(i, j int) bool

type IdxSlice struct {
	idx []int
	cmp Cmp
}

func NewIdxSlice(n int, cmp Cmp) (ret IdxSlice) {
	ret.idx = make([]int, n)
	ret.cmp = cmp
	for i := range ret.idx {
		ret.idx[i] = i
	}
	return
}
func (ix *IdxSlice) Len() int {
	return len(ix.idx)
}
func (ix *IdxSlice) Less(i, j int) bool {
	return ix.cmp(ix.idx[i], ix.idx[j])
}
func (ix *IdxSlice) Swap(i, j int) {
	tmp := *ix
	tmp.idx[i], tmp.idx[j] = tmp.idx[j], tmp.idx[i]
	*ix = tmp
}

func max64(a, b int64) int64 {
	if a > b {
		return a
	}
	return b
}
func solve(n int64, cs []int64) (ans int64) {
	calc := func(e int64) int64 {
		return (e*1e4/n + 50) / 100
	}
	rems := n
	xs := make([]int64, len(cs))
	for i, c := range cs {
		rems -= c
		p := calc(c)
		for x := int64(1); ; x++ {
			e := c + x
			r := calc(e)
			if r > p {
				xs[i] = x
				// fmt.Println(c, e, p, r)
				break
			}
		}
	}
	// fmt.Println("rems", rems)
	ix := NewIdxSlice(len(cs), func(i, j int) bool {
		return xs[i] < xs[j]
	})
	sort.Sort(&ix)
	idx := ix.idx

	// fmt.Println(cs)
	// fmt.Println(xs)
	for _, i := range idx {
		x := xs[i]
		c := cs[i]
		if x > rems {
			break
		}
		bf := calc(c) + max64(calc(rems/2)+calc(rems-rems/2), calc(rems))
		remsx := rems - x
		af := calc(c+x) + max64(calc(remsx/2)+calc(remsx-remsx/2), calc(remsx))
		if af > bf {
			cs[i] = c + x
			rems -= x
		}
	}
	if calc(rems/2)+calc(rems-rems/2) > calc(rems) {
		cs = append(cs, rems/2)
		cs = append(cs, rems-rems/2)
	} else {
		cs = append(cs, rems)
	}
	// fmt.Println(cs)
	ans = 0
	for _, c := range cs {
		ans += calc(c)
	}

	return
}

func main() {
	t := readi()
	for i := 0; i < t; i++ {
		n := readi64s()[0]
		cs := readi64s()
		ans := solve(n, cs)
		fmt.Printf("Case #%d: %d", i+1, ans)
		fmt.Println()
	}
}

var scn = bufio.NewScanner(os.Stdin)

func init() {
	const sz = 1e7
	scn.Buffer(make([]byte, sz), sz)
}

func read() string {
	scn.Scan()
	return scn.Text()
}

func readi() int {
	v, _ := strconv.Atoi(read())
	return v
}

func readi64() int64 {
	v, _ := strconv.ParseInt(read(), 10, 64)
	return v
}

func reads() []string {
	return strings.Split(read(), " ")
}

func readis() []int {
	rs := reads()
	is := make([]int, len(rs))
	for i, s := range rs {
		is[i], _ = strconv.Atoi(s)
	}
	return is
}

func readi64s() []int64 {
	rs := reads()
	is := make([]int64, len(rs))
	for i, s := range rs {
		is[i], _ = strconv.ParseInt(s, 10, 64)
	}
	return is
}
