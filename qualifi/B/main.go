package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
)

const BufSize = 5e7

var scn = bufio.NewScanner(os.Stdin)

func init() {
	scn.Buffer(make([]byte, BufSize), BufSize)
}
func gets() string {
	scn.Scan()
	return scn.Text()
}
func geti() (ret int) {
	ret, _ = strconv.Atoi(gets())
	return
}
func getInts() (ret []int) {
	tokens := strings.Split(gets(), " ")
	for _, t := range tokens {
		e, _ := strconv.Atoi(t)
		ret = append(ret, e)
	}
	return
}

func main() {
	t := geti()
	for i := 1; i <= t; i++ {
		fmt.Printf("Case #%d: ", i)
		if ans, ok := solve(); ok {
			fmt.Println("OK")
		} else {
			fmt.Println(ans)
		}
	}
}

func solve() (ans int, ok bool) {
	n := geti()
	vs := getInts()
	evens := []int{}
	odds := []int{}
	for i, v := range vs {
		if i%2 == 0 {
			evens = append(evens, v)
		} else {
			odds = append(odds, v)
		}
	}
	sort.Ints(evens)
	sort.Ints(odds)
	for i := 0; i < n-1; i++ {
		k := i / 2
		if i%2 == 0 {
			if evens[k] > odds[k] {
				ans = i
				return
			}
		} else if k+1 < len(evens) {
			if odds[k] > evens[k+1] {
				ans = i
				return
			}
		}
	}
	ok = true
	return
}
