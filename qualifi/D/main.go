package main

import (
	"fmt"
	"math"
)

func main() {
	var t int
	fmt.Scan(&t)
	for i := 1; i <= t; i++ {
		fmt.Println(fmt.Sprintf("Case #%d:", i))
		fx, fy, fz := solve()
		fmt.Println(fx)
		fmt.Println(fy)
		fmt.Println(fz)
	}
}

type Point3D struct {
	x, y, z float64
}

func (p Point3D) String() string {
	return fmt.Sprintf("%1.10f %1.10f %1.10f", p.x, p.y, p.z)
}

func solve() (fx, fy, fz Point3D) {
	var a float64
	fmt.Scan(&a)
	sqrt3 := math.Sqrt(3)
	cosTh := math.Sqrt(6) / 3
	sinTh := sqrt3 / 3
	r := math.Sqrt2
	befi := 0
	afti := 0
	maxi := 0
	maxs := 0.0
    const dv = 1000
	for i := 0; i < dv + 2; i++ {
		delta := math.Pi * float64(i) / (2 * dv)
		sinDe := math.Sin(delta)
		e := sqrt3 * (cosTh*math.Cos(delta) + sinDe*sinTh) / 2
		fy.z = sinDe / 2
		s := r * (e + fy.z)
		if s > maxs {
			maxi = i
			maxs = s
		}
		if s < a {
			befi = i
		} else if s > a {
			afti = i
			break
		} else {
			befi = i
			afti = i
			break
		}
	}
	if befi >= dv {
		befi = maxi - 1
		afti = maxi + 1
	}
	lo := math.Pi * float64(befi) / (2 * dv)
	hi := math.Pi * float64(afti) / (2 * dv)
	w := (hi + lo) / 2
	for {
		delta := (hi + lo) / 2
		sinDe := math.Sin(delta)
		e := sqrt3 * (cosTh*math.Cos(delta) + sinDe*sinTh) / 2
		fy.z = sinDe / 2
		s := r * (e + fy.z)
		diff := math.Abs(s - a)
		if diff < 1e-8 {
			w = delta
			break
		}
		if s < a {
			lo = delta
		} else {
			hi = delta
		}
	}
	g := math.Sqrt2 / 4
	cosW := math.Cos(w)
	sinW := math.Sin(w)
	fx = Point3D{g, g * sinW, -g * cosW}
	fy = Point3D{0, cosW / 2, sinW / 2}
	fz = Point3D{g, -g * sinW, g * cosW}
	return
}
