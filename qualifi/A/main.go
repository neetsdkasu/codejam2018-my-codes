package main

import (
	"fmt"
)

func main() {
	var t int
	fmt.Scan(&t)
	for i := 1; i <= t; i++ {
		fmt.Printf("Case #%d: ", i)
		if ans, ok := solve(); ok {
			fmt.Println(ans)
		} else {
			fmt.Println("IMPOSSIBLE")
		}
	}
}

func solve() (ans int, ok bool) {
	var d int
	var p string
	fmt.Scan(&d, &p)
	cs := ([]rune)(p)
	n := len(cs)
	dmg := 0
	atk := make([]int, n+1)
	atk[0] = 1
	cnt := 0
	for i, c := range cs {
		if c == 'S' {
			dmg += atk[i]
			atk[i+1] = atk[i]
			cnt++
		} else {
			atk[i+1] = atk[i] * 2
		}
	}
	if cnt > d {
		return
	}
	ok = true
	if dmg <= d {
		return
	}
	for {
		for i := n - 1; i > 0; i-- {
			if cs[i-1] == 'C' && cs[i] == 'S' {
				cs[i-1], cs[i] = cs[i], cs[i-1]
				dmg -= atk[i]
				atk[i] /= 2
				dmg += atk[i]
				ans++
				if dmg <= d {
					return
				}
			}
		}
	}
}
