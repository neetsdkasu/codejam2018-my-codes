package main

import (
	"fmt"
	"os"
)

func main() {
	var t int
	fmt.Scan(&t)
	for i := 0; i < t; i++ {
		solve()
	}
}

func solve() {
	var a, i, j int
	fmt.Scan(&a)
	for x := 2; x < 1000; x += 3 {
		flag := make([]bool, 9)
		cnt := 0
		for {
			fmt.Println(x, 2)
			fmt.Scan(&j, &i)
			if i < 0 && j < 0 {
				os.Exit(0)
			}
			if i == 0 && j == 0 {
				return
			}
			row := (i - 2) + 1
			col := (j - x) + 1
			idx := row*3 + col
			if !flag[idx] {
				flag[idx] = true
				cnt++
				if cnt == 9 {
					break
				}
			}
		}
	}
}
